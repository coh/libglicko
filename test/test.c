#include "glicko2.h"

#include <stdlib.h>
#include <stdio.h>

static inline void dump_array_f(const char* prefix, const float* arr, const size_t count) {
    printf("%s = {\n", prefix);
    for(size_t i=0; i<count; ++i) {
        printf("    %.8f\n", arr[i]);
    }
    printf("}\n");
}

int main(void) {
    Glicko2_Rating* r = glicko2_player_new_from_glicko(1500, 200);
    float ratings_g1[3] = {1400.f, 1550.f, 1700.f};
    float rd_g1[3] = {30.f, 100.f, 300.f};
    const float scores[3] = {1.f, 0.f, 0.f};

    printf("glicko2 from glicko: mu=%.6f, phi=%.6f\n", r->mu, r->phi);

    for(int i=0; i<3; ++i) {
        ratings_g1[i] = glicko2_player_mu_from_glicko(ratings_g1[i]);
        rd_g1[i] = glicko2_player_phi_from_glicko(rd_g1[i]);
    }

    dump_array_f("ratings", ratings_g1, 3);
    dump_array_f("rd", rd_g1, 3);
    dump_array_f("scores", scores, 3);

    if(!glicko2_player_add_results(r, ratings_g1, rd_g1, scores, 3)) {
        printf("failed to add results to glicko2 rating\n");
        return -1;
    }

    Glicko2_Rating* res = glicko2_player_compute_new_rating(r, 0.5f);

    printf("%.4f\n", res->mu);
    printf("%.4f\n", res->phi);
}
