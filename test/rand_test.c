#include "glicko2.h"

#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

static inline void dump_array_f(const char* prefix, const float* arr,
                                const size_t count) {
  printf("%s = {\n", prefix);
  for (size_t i = 0; i < count; ++i) {
    printf("    %.8f\n", arr[i]);
  }
  printf("}\n");
}

#define COUNT 16

int main(int argc, char** argv) {
  time_t seed = time(NULL);

  if (argc > 1) {
    unsigned long temp = strtoul(argv[1], NULL, 10);
    if (temp > UINT_MAX) {
      printf("seed value is too large.\n");
      return -1;
    }
    seed = temp;
  }

  Glicko2_Rating* r = glicko2_player_new_from_glicko(1500, 200);
  float ratings_g1[COUNT];
  float rd_g1[COUNT];
  float scores[COUNT];

  printf("using seed = %u\n", seed);

  srand(seed);

  for (int i = 0; i < COUNT; ++i) {
    ratings_g1[i] = 1500.f - 512.f + rand() % 1024;
    do {
      rd_g1[i] = rand() % 256;
    } while (rd_g1[i] < 50.f || rd_g1[i] > 350.f);
    int temp;
    do {
      temp = rand() % 4;
    } while (temp > 2);
    switch (temp) {
      case 0:
        scores[i] = 1.0f;
        break;
      case 1:
        scores[i] = 0.5f;
        break;
      case 2:
        scores[i] = 0.0f;
    }
  }

  printf("glicko2 from glicko: mu=%.6f, phi=%.6f\n", r->mu, r->phi);

  for (int i = 0; i < COUNT; ++i) {
    ratings_g1[i] = glicko2_player_mu_from_glicko(ratings_g1[i]);
    rd_g1[i] = glicko2_player_phi_from_glicko(rd_g1[i]);
  }

  dump_array_f("ratings", ratings_g1, COUNT);
  dump_array_f("rd", rd_g1, COUNT);
  dump_array_f("scores", scores, COUNT);

  if (!glicko2_player_add_results(r, ratings_g1, rd_g1, scores, COUNT)) {
    printf("failed to add results to glicko2 rating\n");
    return -1;
  }

  Glicko2_Rating* res = glicko2_player_compute_new_rating(r, 0.5f);

  printf("%.4f\n", res->mu);
  printf("%.4f\n", res->phi);
}
