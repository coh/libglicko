#pragma once

#include <math.h>
#include <stdalign.h>
#include <stdbool.h>
#include <stddef.h>

static const float glicko2_scale_factor =
    173.71779276130072844353f;  // (400 / log(10))

static const float glicko2_default_mu = 0.f;
static const float glicko2_default_phi =
    2.01476192474365234375f;  // 350 / (400 / ln(10))
static const float glicko2_default_sigma = 0.06f;
static const float glicko2_default_tau = 0.2f;

static const float epsilon = 0.000001f;  // sigma-prime convergence threshold

struct Glicko2_Results {
  float* other_mus;
  float* other_phis;
  float* outcomes;
  size_t count, size;
};
typedef struct Glicko2_Results Glicko2_Results;

struct Glicko2_Rating {
  float mu;   // rating
  float phi;  // RD
  Glicko2_Results results;
  float sigma;  // volatility
  float _alignment_;
};
typedef struct Glicko2_Rating Glicko2_Rating;

Glicko2_Rating* glicko2_player_new(void);
Glicko2_Rating* glicko2_player_new_from_glicko(const float rating,
                                               const float RD);
float glicko2_player_mu_from_glicko(const float rating);
float glicko2_player_phi_from_glicko(const float rd);
float glicko2_player_rating_to_glicko(Glicko2_Rating*);
float glicko2_player_RD_to_glicko(Glicko2_Rating*);
void glicko2_player_destroy(Glicko2_Rating*);

bool glicko2_player_add_result(Glicko2_Rating* gr, const float other_mu,
                               const float other_phi, const float outcome);
bool glicko2_player_add_results(Glicko2_Rating* gr, const float* other_mus,
                                const float* other_phis, const float* outcomes,
                                size_t count);
Glicko2_Rating* glicko2_player_compute_new_rating(const Glicko2_Rating*,
                                                  const float tau);
