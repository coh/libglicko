CC=clang
CXX=clang++

CFLAGS?=-W -Wall -Wextra -O3 -mtune=native -std=c11 -fPIC -Iinclude/ -Isrc/include/
CXXFLAGS?=-W -Wall -Wextra -O3 -mtune=native -std=c++11 -fPIC -Iinclude/ -Isrc/include/

DEBUG_FLAGS?=-g -ggdb -DGLICKO_DEBUG

DEBUG?=

ifdef DEBUG
	CFLAGS+=$(DEBUG_FLAGS)
endif

SECRET_SAUCE=

SRC_COMMON := $(wildcard src/common/*.c)
SRC_AVX2 := $(wildcard src/avx2/*.c)
SRC_SSE2 := $(wildcard src/sse2/*.c)
SRC_REF := $(wildcard src/ref/*.c)

OBJS_COMMON = $(SRC_COMMON:.c=.o)
OBJS_REF = $(OBJS_COMMON) $(SRC_REF:.c=.o) 
OBJS_SSE2 = $(OBJS_REF) $(SRC_SSE2:.c=.o) 
OBJS_AVX2 = $(OBJS_SSE2) $(SRC_AVX2:.c=.o) 

LIB_SUFFIX?=.so
LIB_NAME?=libglicko$(LIB_SUFFIX)

all: ref

clean:
	$(RM) $(OBJS_COMMON) $(OBJS_AVX2) $(OBJS_SSE2) $(OBJS_REF) $(wildcard src/private/*.o)

.PHONY: all clean show

API_TYPE=

### this doesn't work. meh. ###
#OBJS=$(OBJS_COMMON) $(OBJS_REF)
#ref: do_link
#
#avx2: override OBJS += $(OBJS_SSE2) $(OBJS_AVX2)
#avx2: override API_CALL=glicko2_player_compute_new_rating_avx2
#avx2: do_link
#
#sse2: override OBJS += $(OBJS_SSE2)
#sse2: export API_CALL=glicko2_player_compute_new_rating_sse2
#sse2: do_link
#
#do_link: $(OBJS)
#	@echo $(OBJS)
#	$(CC) $(CFLAGS) -shared -o $(LIB_NAME) $^

ref: $(OBJS_REF) src/private/glicko2_player_compute_new_rating_ref.o
	$(CC) $(CFLAGS) -shared -o $(LIB_NAME) $^

avx2: override CFLAGS += -mavx2 -msse2
avx2: override API_TYPE=-DAVX2
avx2: $(OBJS_AVX2) src/private/glicko2_player_compute_new_rating_avx2.o
	$(CC) $(CFLAGS) -shared -o $(LIB_NAME) $^

sse2: override CFLAGS += -msse2
sse2: override API_TYPE=-DSSE2
sse2: $(OBJS_SSE2) src/private/glicko2_player_compute_new_rating_sse2.o
	$(CC) $(CFLAGS) -shared -o $(LIB_NAME) $^

.PHONY: ref avx2 sse2

show:
	@echo common: $(SRC_COMMON) "|" $(OBJS_COMMON)
	@echo ref   : $(SRC_REF) "|" $(OBJS_REF)
	@echo sse2  : $(SRC_SSE2) "|" $(OBJS_SSE2)
	@echo avx2  : $(SRC_AVX2) "|" $(OBJS_AVX2)

%.o:%.c
	@echo [CC] $<
	$(CC) $(CFLAGS) $(API_TYPE) -c -o $@ $<

