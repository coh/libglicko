#include "glicko2_avx2.h"
#include "glicko2_sse2.h"

#include <immintrin.h>

float glicko2_compute_d_outcome_avx2(const float* gs, const float* es,
                                     const float* outcomes,
                                     const size_t count) {
  if (count == 0) {
    return 0.f;
  }

  __m256 g_chunk, o_chunk, e_chunk;
  __m256 accumulator = _mm256_set1_ps(0.f);
  size_t i = 0;

  for (; i < count; i += 8) {
    g_chunk = _mm256_loadu_ps(gs + i);
    o_chunk = _mm256_loadu_ps(outcomes + i);
    e_chunk = _mm256_loadu_ps(es + i);
    const __m256 diff = _mm256_sub_ps(o_chunk, e_chunk);
    const __m256 partial = _mm256_mul_ps(g_chunk, diff);
    accumulator = _mm256_add_ps(accumulator, partial);
  }

  float acc[8];
  _mm256_storeu_ps(acc, accumulator);
  float res =
      acc[0] + acc[1] + acc[2] + acc[3] + acc[4] + acc[5] + acc[6] + acc[7];

  if (count % 8 != 0) {
    i -= 8;
    res +=
        glicko2_compute_d_outcome_sse2(gs + i, es + i, outcomes + i, count % 8);
  }

  return res;
}
