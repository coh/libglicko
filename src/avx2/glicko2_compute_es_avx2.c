#include "glicko2_sse2.h"
#include "glicko2_avx2.h"

#include <math.h>

#include <immintrin.h>

void glicko2_compute_es_avx2(float* es, const float mu, const float* other_mus,
                             const float* gs, const size_t count) {
  if (count == 0) {
    return;
  }
#if 0
    __m256 mu_chunk, omu_chunk, g_chunk;
    const __m256 m_one = _mm256_set1_ps(-1.f);
    size_t i=0;

    for(; i<count; i+=8) {
        omu_chunk = _mm256_loadu_ps(other_mus+i);
        g_chunk = _mm256_loadu_ps(gs+i);
        g_chunk = _mm256_mul_ps(g_chunk, m_one); //gs to -gs
        mu_chunk = _mm256_set1_ps(mu);
        mu_chunk = _mm256_sub_ps(mu_chunk, omu_chunk); //mu-other_mu
        g_chunk = _mm256_mul_ps(g_chunk, mu_chunk); //-gs*(mu-other_mu)
        _mm256_storeu_ps(es+i, g_chunk);
        for(size_t k=0;k<8;++k) {
            es[i+k] = 1.0f / (1.0f + expf(es[i+k]));
        }
    }

    i-=8;

    glicko2_compute_es_sse2(es+i, mu, other_mus+i, gs+i, count%8);
#else
  glicko2_compute_es_sse2(es, mu, other_mus, gs, count);
#endif
}
