#include "glicko2_sse2.h"
#include "glicko2_avx2.h"

#include <math.h>

#include <immintrin.h>

#define M_PISQ_F 9.86960440108935799230f

void glicko2_compute_gs_avx2(float* gs, const float* phis, const size_t count) {
  if (count == 0) {
    return;
  }

  __m256 phi_chunk;
  const __m256 three = _mm256_set1_ps(3.0f);
  const __m256 pi_sq = _mm256_set1_ps(M_PISQ_F);
  const __m256 one = _mm256_set1_ps(1.0f);
  size_t i = 0;

  for (; i < count; i += 8) {
    phi_chunk = _mm256_loadu_ps(phis + i);
    phi_chunk = _mm256_mul_ps(phi_chunk, phi_chunk);
    phi_chunk = _mm256_mul_ps(phi_chunk, three);
    phi_chunk = _mm256_div_ps(phi_chunk, pi_sq);
    phi_chunk = _mm256_add_ps(phi_chunk, one);
    phi_chunk = _mm256_sqrt_ps(phi_chunk);
    phi_chunk = _mm256_div_ps(one, phi_chunk);
    _mm256_storeu_ps(gs + i, phi_chunk);
  }

  i -= 8;

  glicko2_compute_gs_sse2(gs + i, phis + i, count % 8);
}
