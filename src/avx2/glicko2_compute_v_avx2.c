#include "glicko2_sse2.h"
#include "glicko2_avx2.h"

#include <immintrin.h>

float glicko2_compute_v_avx2(const float* gs, const float* es,
                             const size_t count) {
  if (count == 0) {
    return 0.f;
  }

  __m256 accumulator = _mm256_set1_ps(0.f);
  __m256 g_chunk, e_chunk, one;
  size_t i = 0;

  for (; i < count; i += 8) {
    e_chunk = _mm256_loadu_ps(es + i);
    g_chunk = _mm256_loadu_ps(gs + i);
    one = _mm256_set1_ps(1.f);
    const __m256 neg = _mm256_sub_ps(one, e_chunk);
    const __m256 g_sq = _mm256_mul_ps(g_chunk, g_chunk);
    const __m256 both = _mm256_mul_ps(e_chunk, neg);
    const __m256 partial = _mm256_mul_ps(both, g_sq);
    accumulator = _mm256_add_ps(accumulator, partial);
  }

  float acc[8];
  _mm256_storeu_ps(acc, accumulator);
  float res =
      acc[0] + acc[1] + acc[2] + acc[3] + acc[4] + acc[5] + acc[6] + acc[7];

  if (count % 8 != 0) {
    i -= 8;

    for (size_t k = 0; k < (count % 8); ++k) {
      res += gs[i + k] * gs[i + k] * es[i + k] * (1.f - es[i + k]);
    }
  }

  return 1.0f / res;
}
