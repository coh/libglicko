#include "glicko2_sse2.h"
#include "glicko2_avx2.h"

#include <math.h>

float glicko2_compute_sigma_p_avx2(const float upper, const float lower,
                                   const float a, const float tau) {
  return glicko2_compute_sigma_p_sse2(upper, lower, a, tau);
}
