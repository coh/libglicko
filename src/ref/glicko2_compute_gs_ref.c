#include "glicko2_ref.h"

#include <math.h>

#define M_PISQ_F 9.86960440108935799230f

void glicko2_compute_gs_ref(float* gs, const float* phis, const size_t count) {
  if (count == 0) {
    return;
  }

  for (size_t i = 0; i < count; ++i) {
    gs[i] = 1.0f / sqrtf(1.0f + (3.0f * phis[i] * phis[i] / M_PISQ_F));
  }
}
