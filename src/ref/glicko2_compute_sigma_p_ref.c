#include "glicko2_ref.h"
#include "glicko2.h"

#include <math.h>

#ifdef GLICKO_DEBUG
#include <stdio.h>
#endif

static float f(const float x, const float upper, const float lower,
               const float a, const float tau) {
  const float ex = expf(x);
  return (ex * (upper - ex)) / (2.f * (lower + ex) * (lower + ex)) -
         (x - a) / (tau * tau);
}

float glicko2_compute_sigma_p_ref(const float upper, const float lower,
                                  const float a, const float tau) {
  float A = roundf(a * 100000.f) / 100000.f;
  float B;
  if ((upper + lower) > lower) {
    B = logf(upper);
  } else {
    int k = 1;
    for (; f(a - k * tau, upper, lower, a, tau) < 0; ++k)
      ;
    B = a - k * tau;
  }

  float fA = f(A, upper, lower, a, tau);
  float fB = f(B, upper, lower, a, tau);
  float C, fC;
#ifdef GLICKO_DEBUG
  printf("%.9f\t%.9f\t%.12f\t%.12f\n", A, B, fA, fB);
#endif

  while (fabsf(B - A) > epsilon) {
    C = A + (A - B) * fA / (fB - fA);
    fC = f(C, upper, lower, a, tau);
    if (fC * fB < 0) {
      A = B;
      fA = fB;
    } else {
      fA /= 2.;
    }
    B = C;
    fB = fC;
#ifdef GLICKO_DEBUG
    printf("%.9f\t%.9f\t%.12f\t%.12f\n", A, B, fA, fB);
#endif
  }

  return expf(A / 2.f);
}
