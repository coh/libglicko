#include "glicko2_ref.h"

float glicko2_compute_d_outcome_ref(const float* gs, const float* es,
                                    const float* outcomes, const size_t count) {
  if (count == 0) {
    return 0.f;
  }

  float ret = 0.f;

  for (size_t i = 0; i < count; ++i) {
    ret += gs[i] * (outcomes[i] - es[i]);
  }

  return ret;
}
