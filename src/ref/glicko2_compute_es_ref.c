#include "glicko2_ref.h"

#include <math.h>

void glicko2_compute_es_ref(float* es, const float mu, const float* other_mus,
                            const float* gs, const size_t count) {
  if (count == 0) {
    return;
  }

  for (size_t i = 0; i < count; ++i) {
    es[i] = 1.0f / (1.0f + expf(-gs[i] * (mu - other_mus[i])));
  }
}
