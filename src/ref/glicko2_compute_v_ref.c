#include "glicko2_ref.h"

float glicko2_compute_v_ref(const float* gs, const float* es,
                            const size_t count) {
  if (count == 0) {
    return 0.f;
  }

  float res = 0.f;

  for (size_t i = 0; i < count; ++i) {
    res += gs[i] * gs[i] * es[i] * (1.f - es[i]);
  }

  return 1.0f / res;
}
