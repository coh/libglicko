#include "api_private.h"
#include "glicko2_sse2.h"

#include <stdlib.h>

#ifdef GLICKO_DEBUG

#include <stdio.h>

static inline void dump_array_f(const char* prefix, const float* arr,
                                const size_t count) {
  printf("%s = {\n", prefix);
  for (size_t i = 0; i < count; ++i) {
    printf("    %.8f\n", arr[i]);
  }
  printf("}\n");
}

static inline void print_value_f(const char* prefix, const float value) {
  printf("%s = %.6f\n", prefix, value);
}

#endif

Glicko2_Rating* glicko2_player_compute_new_rating_sse2(const Glicko2_Rating* gr,
                                                       const float tau) {
  const size_t count = gr->results.count;
  Glicko2_Rating* ret = glicko2_player_new();
  if (ret == NULL) {
    return NULL;
  }

  if (count == 0) {
    const float phi_prime = sqrtf(gr->phi * gr->phi + gr->sigma * gr->sigma);
    ret->mu = gr->mu;
    ret->phi = phi_prime;
    ret->sigma = gr->sigma;
    return ret;
  }

  float* gs = malloc(count * sizeof(float) / sizeof(char));
  float* es = malloc(count * sizeof(float) / sizeof(char));

  if (gs == NULL || es == NULL) {
    if (gs != NULL) {
      free(gs);
    }
    if (es != NULL) {
      free(es);
    }
    return NULL;
  }

  glicko2_compute_gs_sse2(gs, gr->results.other_phis, count);
  glicko2_compute_es_sse2(es, gr->mu, gr->results.other_mus, gs, count);
  const float v = glicko2_compute_v_sse2(gs, es, count);
  const float d =
      glicko2_compute_d_outcome_sse2(gs, es, gr->results.outcomes, count);
  const float delta = v * d;
  const float upper = powf(delta, 2.f) - powf(gr->phi, 2.f) - v;
  const float lower = powf(gr->phi, 2.f) + v;
  const float sigma_prime = glicko2_compute_sigma_p_sse2(
      upper, lower, logf(powf(gr->sigma, 2.f)), tau);
  const float phi_star = sqrtf(gr->phi * gr->phi + sigma_prime * sigma_prime);

  const float phi_prime = 1.f / sqrtf(1.f / (phi_star * phi_star) + 1.f / v);
  const float mu_prime = gr->mu + phi_prime * phi_prime * d;

#ifdef GLICKO_DEBUG
  dump_array_f("gs", gs, count);
  dump_array_f("es", es, count);
  print_value_f("v", v);
  print_value_f("delta", delta);
#endif

  ret->mu = mu_prime;
  ret->phi = phi_prime;

  free(gs);
  free(es);

  return ret;
}
