#include "glicko2_sse2.h"
#include "glicko2_ref.h"

#include <emmintrin.h>

float glicko2_compute_d_outcome_sse2(const float* gs, const float* es,
                                     const float* outcomes,
                                     const size_t count) {
  if (count == 0) {
    return 0.f;
  }

  __m128 g_chunk, o_chunk, e_chunk;
  __m128 accumulator = _mm_set1_ps(0.f);
  size_t i = 0;

  for (; i < count; i += 4) {
    g_chunk = _mm_loadu_ps(gs + i);
    o_chunk = _mm_loadu_ps(outcomes + i);
    e_chunk = _mm_loadu_ps(es + i);
    const __m128 diff = _mm_sub_ps(o_chunk, e_chunk);
    const __m128 partial = _mm_mul_ps(g_chunk, diff);
    accumulator = _mm_add_ps(accumulator, partial);
  }

  float acc[4];
  _mm_storeu_ps(acc, accumulator);
  float res = acc[0] + acc[1] + acc[2] + acc[3];

  if (count % 4 != 0) {
    i -= 4;
    res +=
        glicko2_compute_d_outcome_ref(gs + i, es + i, outcomes + i, count % 4);
  }

  return res;
}
