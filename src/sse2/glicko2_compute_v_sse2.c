#include "glicko2_ref.h"
#include "glicko2_sse2.h"

#include <xmmintrin.h>

float glicko2_compute_v_sse2(const float* gs, const float* es,
                             const size_t count) {
  if (count == 0) {
    return 0.f;
  }

  __m128 accumulator = _mm_set1_ps(0.f);
  __m128 g_chunk, e_chunk, one;
  size_t i = 0;

  for (; i < count; i += 4) {
    e_chunk = _mm_loadu_ps(es + i);
    g_chunk = _mm_loadu_ps(gs + i);
    one = _mm_set1_ps(1.f);
    const __m128 neg = _mm_sub_ps(one, e_chunk);
    const __m128 g_sq = _mm_mul_ps(g_chunk, g_chunk);
    const __m128 both = _mm_mul_ps(e_chunk, neg);
    const __m128 partial = _mm_mul_ps(both, g_sq);
    accumulator = _mm_add_ps(accumulator, partial);
  }

  float acc[4];
  _mm_storeu_ps(acc, accumulator);
  float res = acc[0] + acc[1] + acc[2] + acc[3];

  if (count % 4 != 0) {
    i -= 4;

    for (size_t k = 0; k < (count % 4); ++k) {
      res += gs[i + k] * gs[i + k] * es[i + k] * (1.f - es[i + k]);
    }
  }

  return 1.0f / res;
}
