#include "glicko2_ref.h"
#include "glicko2_sse2.h"

#include <math.h>

#include <xmmintrin.h>

void glicko2_compute_es_sse2(float* es, const float mu, const float* other_mus,
                             const float* gs, const size_t count) {
  if (count == 0) {
    return;
  }
#if 1
  __m128 g_chunk;
  const __m128 m_one = _mm_set1_ps(-1.f);
  size_t i = 0;

  for (; i < count; i += 4) {
    const __m128 omu_chunk = _mm_loadu_ps(other_mus + i);
    g_chunk = _mm_loadu_ps(gs + i);
    g_chunk = _mm_mul_ps(g_chunk, m_one);  // gs to -gs
    __m128 mu_chunk = _mm_set1_ps(mu);
    mu_chunk = _mm_sub_ps(mu_chunk, omu_chunk);  // mu-other_mu
    g_chunk = _mm_mul_ps(g_chunk, mu_chunk);  //-gs * (mu-other_mu)
    _mm_storeu_ps(es + i, g_chunk);
    for (size_t k = 0; k < 4; ++k) {
      es[i + k] = 1.0f / (1.f + expf(es[i + k]));
    }
  }

  i -= 4;

  glicko2_compute_es_ref(es + i, mu, other_mus + i, gs + i, count % 4);
#else
  glicko2_compute_es_ref(es, mu, other_mus, gs, count);
#endif
}
