#include "glicko2_ref.h"
#include "glicko2_sse2.h"

#include <math.h>

#include <xmmintrin.h>

#define M_PISQ_F 9.86960440108935799230f

void glicko2_compute_gs_sse2(float* gs, const float* phis, const size_t count) {
  if (count == 0) {
    return;
  }

  __m128 phi_chunk;
  const __m128 three = _mm_set1_ps(3.0f);
  const __m128 pi_sq = _mm_set1_ps(M_PISQ_F);
  const __m128 one = _mm_set1_ps(1.0f);
  size_t i = 0;

  for (; i < count; i += 4) {
    phi_chunk = _mm_loadu_ps(phis + i);
    phi_chunk = _mm_mul_ps(phi_chunk, phi_chunk);
    phi_chunk = _mm_div_ps(phi_chunk, pi_sq);
    phi_chunk = _mm_mul_ps(phi_chunk, three);
    phi_chunk = _mm_add_ps(phi_chunk, one);
    phi_chunk = _mm_sqrt_ps(phi_chunk);
    phi_chunk = _mm_div_ps(one, phi_chunk);
    _mm_storeu_ps(gs + i, phi_chunk);
  }

  i -= 4;

  glicko2_compute_gs_ref(gs + i, phis + i, count % 4);
}
