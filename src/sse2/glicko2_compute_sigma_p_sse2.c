#include "glicko2_ref.h"
#include "glicko2_sse2.h"

#include <math.h>

float glicko2_compute_sigma_p_sse2(const float upper, const float lower,
                                   const float a, const float tau) {
  return glicko2_compute_sigma_p_ref(upper, lower, a, tau);
}
