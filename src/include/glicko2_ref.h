#pragma once

#include <stddef.h>

void glicko2_compute_gs_ref(float* gs, const float* phis, const size_t count);
void glicko2_compute_es_ref(float* es, const float mu, const float* other_mus,
                            const float* gs, const size_t count);
float glicko2_compute_v_ref(const float* gs, const float* es,
                            const size_t count);
float glicko2_compute_d_outcome_ref(const float* gs, const float* es,
                                    const float* outcomes, const size_t count);
float glicko2_compute_sigma_p_ref(const float upper, const float lower,
                                  const float a, const float tau);
