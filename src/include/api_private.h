#pragma once

#include "glicko2.h"

#ifdef AVX2
Glicko2_Rating* glicko2_player_compute_new_rating_avx2(const Glicko2_Rating*,
                                                       const float tau);
#define GLICKO2_UPDATE_CALL glicko2_player_compute_new_rating_avx2
#elif defined(SSE2)
Glicko2_Rating* glicko2_player_compute_new_rating_sse2(const Glicko2_Rating*,
                                                       const float tau);
#define GLICKO2_UPDATE_CALL glicko2_player_compute_new_rating_sse2
#else
Glicko2_Rating* glicko2_player_compute_new_rating_ref(const Glicko2_Rating*,
                                                      const float tau);
#define GLICKO2_UPDATE_CALL glicko2_player_compute_new_rating_ref
#endif
