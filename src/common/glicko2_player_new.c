#include "glicko2.h"

#include <stdlib.h>

Glicko2_Rating* glicko2_player_new(void) {
  Glicko2_Rating* res = malloc(sizeof(Glicko2_Rating));

  if (res != NULL) {
    res->mu = glicko2_default_mu;
    res->phi = glicko2_default_phi;
    res->sigma = glicko2_default_sigma;
    res->results.other_mus = NULL;
    res->results.other_phis = NULL;
    res->results.outcomes = NULL;
    res->results.count = 0;
    res->results.size = 0;
  }

  return res;
}
