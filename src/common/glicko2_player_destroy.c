#include "glicko2.h"

#include <stdlib.h>

void glicko2_player_destroy(Glicko2_Rating* r) {
  if (r == NULL) {
    return;
  }

  if (r->results.other_mus != NULL) {
    free(r->results.other_mus);
  }

  if (r->results.other_phis != NULL) {
    free(r->results.other_phis);
  }

  if (r->results.outcomes != NULL) {
    free(r->results.outcomes);
  }

  free(r);
  r = NULL;
}
