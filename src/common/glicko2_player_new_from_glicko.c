#include "glicko2.h"

Glicko2_Rating* glicko2_player_new_from_glicko(const float rating,
                                               const float RD) {
  Glicko2_Rating* ret = glicko2_player_new();

  if (ret == NULL) {
    return ret;
  }

  ret->mu = (rating - 1500.f) / glicko2_scale_factor;
  ret->phi = RD / glicko2_scale_factor;

  return ret;
}
