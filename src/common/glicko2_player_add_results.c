#include "glicko2.h"

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

bool glicko2_player_add_results(Glicko2_Rating* gr, const float* other_mus,
                                const float* other_phis, const float* outcomes,
                                size_t count) {
  if (!(gr->results.size >= (gr->results.count + count) &&
        (gr->results.size - count) >= gr->results.count)) {
    size_t new_size = gr->results.size + count;

    if (SIZE_MAX / (sizeof(float) / sizeof(char)) < new_size) {
      return false;
    }

    const size_t alloc_size = new_size * (sizeof(float) / sizeof(char));

    float* tmp = realloc(gr->results.other_mus, alloc_size);

    if (tmp == NULL) {
      return false;
    }
    gr->results.other_mus = tmp;

    tmp = realloc(gr->results.other_phis, alloc_size);
    if (tmp == NULL) {
      return false;
    }
    gr->results.other_phis = tmp;

    tmp = realloc(gr->results.outcomes, alloc_size);
    if (tmp == NULL) {
      return false;
    }
    gr->results.outcomes = tmp;

    gr->results.size = new_size;
  }

  const size_t copy_size = count * (sizeof(float) / sizeof(char));

  memcpy(gr->results.other_mus + gr->results.count, other_mus, copy_size);
  memcpy(gr->results.other_phis + gr->results.count, other_phis, copy_size);
  memcpy(gr->results.outcomes + gr->results.count, outcomes, copy_size);

  gr->results.count += count;

  return true;
}
