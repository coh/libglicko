#include "glicko2.h"

#include "api_private.h"

Glicko2_Rating* glicko2_player_compute_new_rating(const Glicko2_Rating* gr,
                                                  const float tau) {
  return GLICKO2_UPDATE_CALL(gr, tau);
}
