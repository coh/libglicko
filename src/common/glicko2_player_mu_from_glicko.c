#include "glicko2.h"

float glicko2_player_mu_from_glicko(const float rating) {
  return (rating - 1500.f) / glicko2_scale_factor;
}
