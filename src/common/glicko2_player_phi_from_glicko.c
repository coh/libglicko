#include "glicko2.h"

float glicko2_player_phi_from_glicko(const float RD) {
  return RD / glicko2_scale_factor;
}
