#include "glicko2.h"

bool glicko2_player_add_result(Glicko2_Rating* gr, const float other_mu,
                               const float other_phi, const float outcome) {
  return glicko2_player_add_results(gr, &other_mu, &other_phi, &outcome, 1);
}
