#include "glicko2.h"

float glicko2_player_rating_to_glicko(Glicko2_Rating* gr) {
  if (gr == NULL) {
    return 0.f;
  }

  return glicko2_scale_factor * gr->mu + 1500.f;
}
